package org.tyoda.wurm.shorterfemalesleeves;

import org.gotti.wurmunlimited.modloader.interfaces.Versioned;
import org.gotti.wurmunlimited.modloader.interfaces.WurmServerMod;

public class Boop implements WurmServerMod {
    public static final String version = "1.0";
    @Override
    public String getVersion(){
        return version;
    }
}
